<?php


class Site extends CoreSite {

	protected $useDefaultTheme = false;

	public function __construct() {
		parent::__construct();
	}

	static public function get_acf_value($acf, $node) {
		//
		if (!isset($acf[$node]))
			return [];
		//
		return $acf[$node]["value"];
	}

}
