<?php

	include __dir__ . "/src/Debug.php";
	include __dir__ . "/src/PolylangTranslationRegister.php";
	include __dir__ . "/src/Request.php";
	include __dir__ . "/src/Config.php";
	include __dir__ . "/src/CoreSite.php";
	// The config per website
	include __dir__ . "/Site.php";
