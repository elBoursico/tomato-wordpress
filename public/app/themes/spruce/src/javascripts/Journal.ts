import * as $ from "jquery";
import PageTransitionManager, { ChangePage } from "./PageTransitionManager";

export default class Journal {

    articles: NodeListOf<Element> = document.querySelectorAll(".journal .article");
    changePage: ChangePage

    constructor(changePage: ChangePage) {

        this.changePage = changePage;
        new Disapear(this.articles);
        new MoveIt(this.articles);

        for (let i = 0; i < this.articles.length; i++) {
            const links = Array.prototype.slice.call(this.articles[i].querySelectorAll("a"));
            links.forEach((item) => {
                item.addEventListener("click", this.handleClickArticle);
            });
        }

    }

    handleClickArticle = (e) => {
        e.preventDefault();
        const el = $(e.currentTarget);
        const urlNewPage = el.attr("href");
        const transition = el.attr("data-type");
        el.parent().parent().addClass("clicked");

        if (!PageTransitionManager.isAnimating) {
            this.changePage(urlNewPage, transition);
        }
    }

}

class DisapearItem {

    el: HTMLElement;
    opacity: number = 1;
    bbox: ClientRect;

    constructor(el: HTMLElement) {
        this.el = el;
        this.bbox = this.el.getBoundingClientRect();
    }

    update = () => {
        const startPoint = 400;
        this.bbox = this.el.getBoundingClientRect();
        const bottom = this.bbox.bottom;

        if (bottom < startPoint && bottom >= 0) {
            this.opacity = bottom / startPoint;
        } else if (bottom < 0) {
            if (this.opacity === 0) {
                return;
            }
            this.opacity = 0;
        } else if (bottom > startPoint) {
            if (this.opacity === 1) {
                return;
            }
            this.opacity = 1;
        }

        this.el.style.opacity = `${this.opacity}`;
    }
}

class Disapear {
    constructor(nodeList) {

        const arrayItem = Array.prototype.slice.call(nodeList);

        const instances: Array<DisapearItem> = [];

        arrayItem.forEach((item) => {
            instances.push(new DisapearItem(item));
        });

        window.addEventListener("scroll", () => {

            instances.forEach((inst) => {
                inst.update();
            }, { passive: true });

        });
    }
}

class MoveIt {

    constructor(nodeList) {

        const arrayItem = Array.prototype.slice.call(nodeList);

        const $window = $(window);
        const instances = [];

        arrayItem.forEach((item) => {
            instances.push(new MoveItItem(item));
        });

        window.addEventListener("scroll", () => {
            const scrollTop = $window.scrollTop();
            instances.forEach((inst) => {
                inst.update(scrollTop);
            }, { passive: true });
        });

    }
}

class MoveItItem {

    el;
    speed: number = Math.floor(Math.random() * 20) + 10;

    constructor(el) {
        this.el = $(el);
        // this.speed = parseInt(this.el.attr('data-scroll-speed'));
    }

    update = (scrollTop) => {
        this.el.css({
            transform: `translateY(${-(scrollTop / this.speed)}px)`,
        });
        // console.log(scrollTop);
    }

}