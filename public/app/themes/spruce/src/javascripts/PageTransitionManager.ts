import * as $ from "jquery";

import Menu from "./Menu";
import Journal from "./Journal";
import { TransitionController } from "./TransitionController";

// resoudre error console about touch event on the slider
// bug when you resize, go to slide 1 without change color

//IDEA: une technique serais de garder le carousel et sa logique sur toute les pages, mais juste de masquer
// la section carousel quand le contenu est loaded

//TODO: refactore this code about management of class for content page transition !!!!!!!!!!!!!!!

export type ChangePage = (url: string, transition: string, isBack?: boolean) => void;

// interface HistoryItem {
//     anim: string
//     url: string
// }

export default class PageTransitionManager {

    public static isAnimating: boolean = false;
    public static body = $("body");

    private selectorMainContent: string = "main-content";
    private currentPage: string;

    private menu: Menu;
    private journal: Journal;

    private lastTransition: string;

    // private history: Array<HistoryItem> = [];

    public constructor(startPage: string) {

        this.currentPage = startPage;
        this.initNewContent(startPage);

        // $(window).on("popstate", this.handlePopstate);

    }

    private initMenu() {
        this.menu = new Menu(this.changePage);
    }

    private initJournal() {
        this.journal = new Journal(this.changePage);
    }

    // private handlePopstate = () => {
    //     console.log(window.history.state);
    //     // console.log(this.history);
    //     const newLocation = location.pathname;
    //     // console.log(newLocation);

    //     // let isBack;
    //     //     if (!this.history[this.history.length - 2]) {
    //     //         console.log("here");
    //     //         isBack = true;
    //     //     } else {
    //     //         isBack = newLocation === this.history[this.history.length - 2].url ? false : true;
    //     //     }

    //     // console.log(isBack);
    //     if (!PageTransitionManager.isAnimating) {
    //         this.changePage(newLocation, "page-transition-menu");
    //     }
    // }


    public changePage: ChangePage = async (url, transition, isBack = false) => {

        PageTransitionManager.isAnimating = true;
        PageTransitionManager.body.addClass("transition");

        const content = await this.loadNewContent(url, transition, isBack);

        switch (true) {

            case transition === "page-transition-menu":

                TransitionController.transitionMenu(content, this.initNewContent.bind(this, this.currentPage), isBack);

                break;

            case transition === "page-transition-article":

                TransitionController.transitionArticle(content, this.initNewContent.bind(this, this.currentPage), isBack);

                break;
        }

    }

    private async loadNewContent(url: string, transition: string, isBack: boolean) {

        const allPage = await $.get(url);
        const main = $(".main-content", allPage);

        const newSectionName = url.replace(".html", "").replace("/", "");
        const previousContent = $(`.${this.selectorMainContent}`);
        main.addClass("notVisible");
        $('main').append(main);
        
        // if (!isBack) {
            window.history.replaceState({
                [newSectionName]: url,
            }, newSectionName, url);
            // console.log("after push", window.history.state);
        // }


        // this.history.push({
        //     anim: transition,
        //     url: url,
        // });

        if (newSectionName.length === 0) {
            this.currentPage = "page-menu";
        } else {
            this.currentPage = `page-${newSectionName}`;
        }

        return {
            previousS: previousContent,
            newS: main
        };
    }

    private initNewContent(page: string) {
        // console.log(page);
        switch (true) {
            case page === "page-menu":
                this.initMenu();
                break;

            case page === "page-journal":
                this.initJournal();
                break;
        }
    }

}