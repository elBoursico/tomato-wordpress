const webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: {
        "js/main": "./src/javascripts/main.ts",
    },
    output: {
        filename: "[name].bundle.js",
        chunkFilename: "[name].bundle.js",
        path: __dirname + "/static/",
        // publicPath: "/dist/"
    },
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "css/style.css",
            allChunks: true
        }),
    ],
    module: {
        rules: [
            {
				test: /\.(ts|tsx|js|jsx)$/,
				use: [
					{
						loader: 'ts-loader',
						options: {
							transpileOnly: true
						}
					}
				]
			},
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                importLoaders: 1,
                                sourceMap: true,
                            }
                        },
                        // "postcss-loader",
                    ]
                }),
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                importLoaders: 2,
                                sourceMap: true,
                                // localIndentName: "[name]__[local]__[hash:base64:5]"
                            }
                        },
                        // "postcss-loader",
                        "sass-loader",
                        // {
                        //     loader: 'sass-resources-loader',
                        //     options: {
                        //         // Provide path to the file with resources
                        //         // resources: './dev/client/stylesheets/mixin_perso.scss',

                        //         // Or array of paths
                        //         resources: [
                        //             './dev/client/stylesheets/mixin.scss',
                        //             './dev/client/stylesheets/mixin_perso.scss',
                        //         ]
                        //     },
                        // },
                    ]
                }),
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg|jpeg)$/,
                loader: 'url-loader?limit=20000&name=images/[name].[ext]'
            },
        ]
    },
};