<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber\Timber::get_context();
$post = Timber\Timber::query_post();
$content = new stdClass();

$acf = get_field_objects($post->ID);

$content->title = $post->title;
$content->header = Site::get_acf_value($acf, "header");
$content->layout = Site::get_acf_value($acf, "layout");
$content->technical_informations = Site::get_acf_value($acf, "technical_informations");
$content->body_color = Site::get_acf_value($acf, "body_color");

// $idNext = Site::get_acf_value($acf, "next_article");

// $acf2 = get_field_objects($idNext);

// s($acf);

$context['post'] = $post;
$context['content'] = $content;

if ( post_password_required( $post->ID ) ) {
	Timber\Timber::render( 'core/single-password.twig', $context );
} else {
	Timber\Timber::render( array( 
		'custom/core/single-' . $post->ID . '.twig', 
		'custom/core/single-' . $post->post_type . '.twig', 
		'custom/core/single.twig',
		'core/single-' . $post->ID . '.twig', 
		'core/single-' . $post->post_type . '.twig', 
		'core/single.twig',
	), $context );
}
